<?php

namespace App\Controller;

use App\Service\CheckVatNumber\TransformStringNumberToDto;
use App\Service\CheckVatNumber\ValidationVatNumber;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\CheckVatNumber\CheckVatNumberService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class VatController extends AbstractController
{
    private CheckVatNumberService $checkVatNumberService;

    public function __construct(CheckVatNumberService $checkVatNumberService)
    {
        $this->checkVatNumberService = $checkVatNumberService;
    }

    #[Route('/vat', name: 'vat', methods: ['GET'])]
    public function index(Request $request): Response
    {
        $vatNumber = $request->get('vatNumber');

        if (!empty($vatNumber)) {
            $validation = new ValidationVatNumber(
                (new TransformStringNumberToDto($vatNumber))->getDto()
            );

            if (empty($validation->getErrorMessage())) {
                $checkVatNumberService = $this->checkVatNumberService;
                $checkVatNumberService->execute($vatNumber);

                if (!$checkVatNumberService->isFail()) {
                    $message = $checkVatNumberService->getMessage();
                } else {
                    $message = 'Что-то пошло не так! Повторите попытку позднее.';
                }
            } else {
                $message = $validation->getErrorMessage();
            }
        } else {
            $message = null;
        }

        return $this->render('vat/form.html.twig', [
            'message' => $message,
        ]);
    }
}
