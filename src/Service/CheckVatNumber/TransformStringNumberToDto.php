<?php

declare(strict_types=1);

namespace App\Service\CheckVatNumber;

use App\Dto\CheckVatNumber\BodyForHttpRequestDto;

final class TransformStringNumberToDto
{
    private string $numberFromUser;

    private BodyForHttpRequestDto $dto;

    public function __construct(string $numberFromUser)
    {
        $this->numberFromUser = $numberFromUser;
        $this->fillDto();
    }

    public function getDto(): BodyForHttpRequestDto
    {
        return $this->dto;
    }

    private function fillDto(): void
    {
        $numberFromUser = $this->numberFromUser;

        $this->dto = new BodyForHttpRequestDto(
            mb_substr($numberFromUser, 0, 2),
            mb_substr($numberFromUser, 2),
        );
    }
}