<?php

declare(strict_types=1);

namespace App\Service\CheckVatNumber;

use App\Dto\CheckVatNumber\BodyForHttpRequestDto;

final class ValidationVatNumber
{
    private const ITEMS = [
        'AT',
        'BE',
        'BG',
        'CY',
        'CZ',
        'DE',
        'DK',
        'EE',
        'EL',
        'ES',
        'FI',
        'FR',
        'HR',
        'HU',
        'IE',
        'IT',
        'LT',
        'LU',
        'LV',
        'MT',
        'NL',
        'PL',
        'PT',
        'RO',
        'SE',
        'SI',
        'SK',
        'XI'
    ];

    private BodyForHttpRequestDto $dto;

    private ?string $errorMessage = null;

    public function __construct(BodyForHttpRequestDto $dto)
    {
        $this->dto = $dto;
        $this->validate();
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    private function validate(): void
    {
        $string = $this->dto->toString();

        if (mb_strlen($string) < 3) {
            $this->errorMessage = 'Номер VAT должен быть не менее 3-х символов!';
            return;
        }

        if (!in_array($this->dto->getMemberStateCode(), self::ITEMS)) {
            $this->errorMessage = 'Недопустимый префикс номера VAT! Белый список: ' . implode(', ', self::ITEMS) . '.';
            return;
        }
    }
}