<?php

declare(strict_types=1);

namespace App\Service\CheckVatNumber;

final class Adapter
{
    private const VALID_FROM_HTML = 'Yes, valid VAT number';

    private const NOT_VALID_FROM_HTML = 'No, invalid VAT number for cross border transactions within the EU (please refer to <a href="faqvies.do">FAQ</a>, questions 7, 11, 12, 13 and 20 for more information).';

    private const VALID_FOR_USER_MESSAGE = 'Your VAT Number has been successfully validated. Your company legal data was updated according to the VIES database.';

    private const NOT_VALID_FOR_USER_MESSAGE = 'The VAT number you are requesting is not valid. Either the Number is not active or not allocated. Please double-check that you are entering the right VAT Number.';

    private string $htmlMessage;

    private ?string $messageForUser = null;

    private bool $isExistsNumber;

    public function __construct(string $htmlMessage)
    {
        $this->htmlMessage = $htmlMessage;
        $this->fillMessageForUser();
    }

    public function getMessageForUser(): string
    {
        return $this->messageForUser;
    }

    public function isExistsNumber(): bool
    {
        return $this->isExistsNumber;
    }

    private function fillMessageForUser(): void
    {
        $isExistsNumber = false;
        $htmlMessage = $this->htmlMessage;

        if ($htmlMessage === self::VALID_FROM_HTML) {
            $isExistsNumber = true;
            $messageForUser = self::VALID_FOR_USER_MESSAGE;
        }

        if ($htmlMessage === self::NOT_VALID_FROM_HTML) {
            $messageForUser = self::NOT_VALID_FOR_USER_MESSAGE;
        }

        $this->isExistsNumber = $isExistsNumber;
        $this->messageForUser = $messageForUser ?? null;
    }
}