<?php

declare(strict_types=1);

namespace App\Service\CheckVatNumber;

use Psr\Log\LoggerInterface;

final class CheckVatNumberService implements CheckVatNumberInterface
{
    private const IS_FAIL_TRUE = true;

    private HttpRequest $httpRequest;

    private LoggerInterface $logger;

    private bool $isFail = false;

    private string $message;

    private bool $isExistsNumber;

    public function __construct(HttpRequest $httpRequest, LoggerInterface $logger)
    {
        $this->httpRequest = $httpRequest;
        $this->logger = $logger;
    }

    public function isFail(): bool
    {
        return $this->isFail;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function isExistsNumber(): bool
    {
        return $this->isExistsNumber;
    }

    public function execute(string $number)
    {
        try {
            $this->checkNumber($number);
        } catch (\Exception $exception) {
            $this->isFail = self::IS_FAIL_TRUE;
            $this->logger->error($exception->getMessage());
            $this->logger->error($exception->getTraceAsString());
        }
    }

    private function checkNumber(string $number): void
    {
        $dto = (new TransformStringNumberToDto($number))->getDto();
        $httpRequest = $this->httpRequest;

        $httpRequest->setBody($dto);
        $httpRequest->execute();

        $content = $httpRequest->getContent();

        if (!empty($content)) {
            $parser  = new ParserHtml($content);
            $adapter = new Adapter($parser->getMessage());
            $message = $adapter->getMessageForUser();

            if (!empty($message)) {
                $this->message = $message;
                $this->isExistsNumber = $adapter->isExistsNumber();
            } else {
                $this->isFail = self::IS_FAIL_TRUE;
                $this->logger->error('CheckVatNumberService: not valid html from response!');
            }
        }
    }
}