<?php

declare(strict_types=1);

namespace App\Service\CheckVatNumber;

interface CheckVatNumberInterface
{
    public function isFail(): bool;

    public function getMessage(): string;

    public function isExistsNumber(): bool;
}