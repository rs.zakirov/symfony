<?php

declare(strict_types=1);

namespace App\Service\CheckVatNumber;

use Symfony\Component\DomCrawler\Crawler;

final class ParserHtml
{
    private string $html;

    public function __construct(string $html)
    {
        $this->html = $html;
    }

    public function getMessage(): string
    {
        $crawler = new Crawler($this->html);
        return $crawler->filter('.layout-content > div')->eq(1)->filter('b > span')->html();
    }
}