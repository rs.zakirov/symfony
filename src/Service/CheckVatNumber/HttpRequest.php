<?php

declare(strict_types=1);

namespace App\Service\CheckVatNumber;

use App\Dto\CheckVatNumber\BodyForHttpRequestDto;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class HttpRequest
{
    private const URL = 'https://ec.europa.eu/taxation_customs/vies/vatResponse.html';

    private HttpClientInterface $client;

    private ResponseInterface $response;

    private BodyForHttpRequestDto $body;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function setBody(BodyForHttpRequestDto $body): void
    {
        $this->body = $body;
    }

    public function execute(): void
    {
        $dto = $this->body;
        $this->response = $this->client->request(
            'POST',
            self::URL,
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'body' => [
                    'memberStateCode' => $dto->getMemberStateCode(),
                    'number' => $dto->getNumber(),
                ]
            ]
        );
    }

    public function getContent(): ?string
    {
        $response = $this->response;

        if ($response->getStatusCode() === 200) {
            $output = $response->getContent();
        } else {
            $output = null;
        }

        return $output;
    }
}