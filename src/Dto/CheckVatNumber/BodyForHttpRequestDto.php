<?php

declare(strict_types=1);

namespace App\Dto\CheckVatNumber;

final class BodyForHttpRequestDto
{
    private string $memberStateCode;

    private string $number;

    public function __construct(string $memberStateCode, string $number)
    {
        $this->memberStateCode = $memberStateCode;
        $this->number = $number;
    }

    public function getMemberStateCode(): string
    {
        return $this->memberStateCode;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function toString(): string
    {
        return "{$this->memberStateCode}{$this->number}";
    }
}